package com.example.recrutement.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class ServicesTest {

    @Autowired
    private Services service;

    @Test
    void ok_case_3() throws Exception {
        // expected
        List<Integer> expectedMoneyBack = List.of(10, 5, 2, 1);
        //actual
        List<Integer> actualMoneyBack = service.moneyBack(2, 20);

        assertEquals(expectedMoneyBack, actualMoneyBack);
    }

    @Test
    void ko_case_1() {
        Exception exception = assertThrows(Exception.class, () -> {
            this.service.moneyBack(3, 2);
        });

        String expectedMessage = "Vous ne m’avez pas donné assez";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void ok_case_2() throws Exception {
        // expected
        List<Integer> expectedMoneyBack = new ArrayList<>();
        //actual
        List<Integer> actualMoneyBack = service.moneyBack(2, 2);

        assertEquals(expectedMoneyBack, actualMoneyBack);
    }

    @Test
    void ok_case_4() throws Exception {
        // expected
        List<Integer> expectedMoneyBack = List.of(20, 10, 5, 2, 1);
        //actual
        List<Integer> actualMoneyBack = service.moneyBack(2, 40);

        assertEquals(expectedMoneyBack, actualMoneyBack);
    }

    @Test
    void ok_case_7() throws Exception {
        //     Entrées : 28, 180
        //    Sorties : [50, 50, 50, 2]
        List<Integer> expectedMoneyBack = List.of(50, 50, 50, 2);
        //actual
        List<Integer> actualMoneyBack = service.moneyBack(28, 180);

        assertEquals(expectedMoneyBack, actualMoneyBack);
    }

    @Test
    void ok_case_6() throws Exception {
        //    Entrées : 2, 30
        //    Sorties : [20, 5, 2 ,1]
        List<Integer> expectedMoneyBack = List.of(20, 5, 2, 1);
        //actual
        List<Integer> actualMoneyBack = service.moneyBack(2, 30);

        assertEquals(expectedMoneyBack, actualMoneyBack);
    }

}