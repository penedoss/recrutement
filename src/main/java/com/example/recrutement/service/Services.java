package com.example.recrutement.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Services {
    public List<Integer> moneyBack(int price, int moneyGiven) throws Exception {
        if (moneyGiven < price)
            throw new Exception("Vous ne m’avez pas donné assez");
        if (moneyGiven == price)
            return new ArrayList<>();
        return computeMoney(price, moneyGiven);
    }

    private List<Integer> computeMoney(int price, int moneyGiven) {
        List<Integer> possibilities = List.of(50, 20, 10, 5, 2, 1);
        List<Integer> moneyBack = new ArrayList<>();
        possibilities.forEach(money -> computeMoneyBack(money, moneyBack, price, moneyGiven));

        return moneyBack;

    }

    private List<Integer> computeMoneyBack(int money, List<Integer> moneyBack, int price, int moneyGiven) {
        int moneyBackSum = moneyBack.stream().mapToInt(Integer::intValue).sum();
        int restMoney = (moneyGiven - price) - moneyBackSum;

        if (moneyBackSum != (moneyGiven - price) && money + moneyBackSum <= (moneyGiven - price)) {
            while (money <= restMoney) {
                moneyBack.add(money);
                restMoney = restMoney - money;
            }
        }
        return moneyBack;
    }
}
