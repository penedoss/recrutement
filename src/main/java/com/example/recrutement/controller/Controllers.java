package com.example.recrutement.controller;

import com.example.recrutement.service.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cashier")
public class Controllers {

    @Autowired
    private Services service;

    @GetMapping("/fund-back")
    public List<Integer> moneyBack(@RequestParam @NonNull int price, @RequestParam @NonNull int moneyGiven) throws Exception {
        return this.service.moneyBack(price, moneyGiven);
    }
}
